# gitlab-pages-react-oauth2-hook

## Introduction 

This project demonstrates a single page app (SPA) hosted on GitLab Pages
featuring [react-oauth2-hook](https://www.npmjs.com/package/react-oauth2-hook)
with [GitLab OAuth2 authentication service provider](https://docs.gitlab.com/ee/integration/oauth_provider.html).

See https://yaegashi.gitlab.io/gitlab-pages-react-oauth2-hook for the actual SPA.
You can log in / log out with your gitlab.com account at any time.
Only `read_user` permission is requested.
Your user information is only exposed to your browser and not shared with anyone.

Thanks to react-oauth2-hook,
the authentication flow (implicit grant) proceeds in another browser tab / window,
and the main SPA sessions and states are always preserved and unaffected by it.
There's no need for conveying the session information (last path visited, for example)
using `state` parameter in the authentication flow.

## Details

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

While the OAuth2 callback endpoint is `/callback` trapped by `BrowserRouter`,
the app utilizes `HashRouter` for its main component paths.  It's because
[GitLab Pages doesn't support SPA path routhing](https://gitlab.com/gitlab-org/gitlab-pages/issues/23) yet.

```jsx
// App component
// Hybrid path routing with BrowserRouter (/callback) and HashRouter (/#/...)
export default () =>
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
      <Route exact path="/callback" component={OAuthCallback} />
      <Route exact path="/" render={() => <HashRouter><AppRoot /></HashRouter>} />
    </Switch>
  </BrowserRouter>
```

In order to handle the OAuth2 callback endpoint access,
the app build script copies `/index.html` to `/callback` in [.gitlab-ci.yml](.gitlab-ci.yml).

## License

MIT
