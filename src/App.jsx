import React from 'react'
import useReactRouter from 'use-react-router'
import urljoin from 'url-join';
import { BrowserRouter, HashRouter, Route, Switch, Link } from 'react-router-dom'
import { useOAuth2Token, OAuthCallback } from 'react-oauth2-hook'

// GitLab OAuth2 configuration
const GitLabURL = 'https://gitlab.com';
const GitLabClientID = '8080ca13e6787c1ac6d0d848b2759a8cfb18cdb986bdc5b7963719398c60aaff';

// Context for GitLab OAuth2 hooks
const GitLabOAuth2Context = React.createContext();

// GitLab OAuth2 service provider hooks
const useGitLabOAuth2 = () => {
  // Create absolute URI to redirect
  const redirectUri = new URL(document.location.href);
  redirectUri.pathname = process.env.PUBLIC_URL + '/callback';
  redirectUri.hash = '';
  redirectUri.search = '';

  // Get React OAuth2 hooks
  const [token, getToken, setToken] = useOAuth2Token({
    authorizeUrl: urljoin(GitLabURL, '/oauth/authorize'),
    scope: ['read_user'],
    clientID: GitLabClientID,
    redirectUri: redirectUri.href,
  });

  // Retrieve user information using GitLab API once token acquired
  const [user, setUser] = React.useState();
  const [error, setError] = React.useState();
  React.useEffect(() => {
    if (token) {
      fetch(urljoin(GitLabURL, '/api/v4/user'), {
        headers: new Headers({ Authorization: 'Bearer ' + token }),
      }).then(
        response => response.json()
      ).then(data => {
        setUser(data);
        setError(null);
      }).catch((error) => {
        setUser(null);
        setError(error)
      });
    }
    else {
      setUser(null);
      setError(null);
    }
  }, [token]);

  // Expose available hooks
  return { token, getToken, setToken, user, error };
}

// App navigation component
const AppNav = () => {
  const { location } = useReactRouter();
  const { user, getToken, setToken } = React.useContext(GitLabOAuth2Context);
  const loginF = (e) => { e.preventDefault(); getToken(); }
  const logoutF = (e) => { e.preventDefault(); setToken(null); }
  const loginLink = user ?
    <a href="/" onClick={logoutF}>Log out</a> :
    <a href="/" onClick={loginF}>Log in with GitLab</a>;
  return (
    <div>
      <h3>Location: {location.pathname}</h3>
      <p>User: {user ? user.username : null} {loginLink}</p>
      <p>
        Navigation:&nbsp;
        <Link to="/">Top</Link>&nbsp;
        <Link to="..">Up</Link>&nbsp;
        <Link to="A/">A</Link>&nbsp;
        <Link to="B/">B</Link>&nbsp;
        <Link to="C/">C</Link>
      </p>
    </div>
  );
}

// App authentication component
const AppAuth = () => {
  const { token, user, error } = React.useContext(GitLabOAuth2Context);
  const tokenPre = token ? <pre>Token: {token}</pre> : null;
  const userPre = user ? <pre>User: {JSON.stringify(user, null, '  ')}</pre> : null;
  const errorPre = error ? <pre>Error: {error}</pre> : null;
  return (
    <div>
      <h3>Authentication</h3>
      {errorPre}
      {userPre}
      {tokenPre}
    </div>
  );
}

// App root component
const AppRoot = () => {
  const auth = useGitLabOAuth2();
  return (
    <GitLabOAuth2Context.Provider value={auth}>
      <AppNav />
      <hr />
      <AppAuth />
    </GitLabOAuth2Context.Provider>
  );
}

// App component
// Hybrid path routing with BrowserRouter (/callback) and HashRouter (/#/...)
export default () =>
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
      <Route exact path="/callback" component={OAuthCallback} />
      <Route exact path="/" render={() => <HashRouter><AppRoot /></HashRouter>} />
    </Switch>
  </BrowserRouter>
